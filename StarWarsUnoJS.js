let restApi = "https://nowaunoweb.azurewebsites.net/api/game";
let game = {};
let topCard = {};
let player1 = {};
let player1Cards = {};
let player1Score;
let player2 = {};
let player2Cards = {};
let player2Score;
let player3 = {};
let player3Cards = {};
let player3Score;
let player4 = {};
let player4Cards = {};
let player4Score;
let p1 = "";
let p2 = "";
let p3 = "";
let p4 = "";
let players = {};
let nextPlayer = "";
let pickedCard = {};
let playedCard = {};
let currentPlayer = "";
let wildColor = null;
let dunkleSeite = $("#vader");
console.log("Variable dunkleSeite: ", dunkleSeite);
let helleSeite = $("#yoda");
console.log("Variable helleSeite: ", helleSeite);

let INIT = {
  onReady: function () {
    $("#inputPlayer1").hide();
    $("#inputPlayer2").hide();
    $("#inputPlayer3").hide();
    $("#inputPlayer4").hide();
    $("#welcome").show(4000);
    INIT.setPlayers();
  },

  setPlayers: function () {
    $("#inputPlayer1").slideToggle(3000);
    $("#inputPlayer1 input").keyup(function (e) {
      if ($(this).val() != "" && e.keyCode == 13) {
        p1 = $(this).val();
        $("#p1").html(p1);
        INIT.setPlayer2();
      }
    });
  },

  setPlayer2: function () {
    $("#inputPlayer1").hide()
    $("#inputPlayer2").show();
    $("#inputPlayer2 input").keyup(function (e) {
      if ($(this).val() != "" && e.keyCode == 13) {
        if ($(this).val() == p1) {
          $('#inputPlayer2 :input').val('');
          $("#welcome #exampleModalCenter").modal();
        } else {
          p2 = $(this).val();
          $("#p2").html(p2);
          INIT.setPlayer3();
        }
      }
    });
  },

  setPlayer3: function () {
    $("#inputPlayer2").hide()
    $("#inputPlayer3").show();
    $("#inputPlayer3").val("");
    $("#inputPlayer3 input").keyup(function (e) {
      if ($(this).val() != "" && e.keyCode == 13) {
        if (($(this).val() == p1) || ($(this).val() == p2)) {
          $('#inputPlayer3 :input').val('');
          $("#welcome #exampleModalCenter").modal();
        } else {
          p3 = $(this).val();
          $("#p3").html(p3);
          INIT.setPlayer4();
        }
      }
    });
  },

  setPlayer4: function () {
    $("#inputPlayer3").hide()
    $("#inputPlayer4").show();
    $("#inputPlayer4").val("");
    $("#inputPlayer4 input").keyup(function (e) {
      if ($(this).val() != "" && e.keyCode == 13) {
        if (($(this).val() == p1) || ($(this).val() == p2) || ($(this).val() == p3)) {
          $('#inputPlayer4 :input').val('');
          $("#welcome #exampleModalCenter").modal();
        } else {
          p4 = $(this).val();
          $("#p4").html(p4);
          $("#inputPlayer4").hide();
          $("#welcome").slideUp(2000);
          $("#welcomeBackground").slideUp(2000);
          if (helleSeite == true) {
            $("#gameBackground").addClass("gameBackgroundHell").fadeIn(2500);
          } else if (helleSeite == false) {
            $("#gameBackground").addClass("gameBackgroundDunkel").fadeIn(2500);
          }
          $("#gameBackground").fadeIn();
          $("#startGame").fadeIn(2500);
          players = [p1, p2, p3, p4];

          INIT.postPlayers();
        }
      }
    });
  },

  postPlayers: function () {
    console.log("------------ Spiel starten -----------------");

    $.ajax({
      method: "POST",
      url: restApi + "/start",
      data: JSON.stringify(players),
      contentType: "application/json",
      success: function (data) {

        game = data;
        gameId = game.Id;
        topCard = game.TopCard;
        nextPlayer = game.NextPlayer;
        INIT.createPlayers();

        $("#p1").html(p1);
        console.log("GameInfo: ", game);

        $("#startgame").fadeIn(1500);
        INIT.showTopCard();
      },
      error: function (err) {
        console.log(err);
      }
    });
  },

  createPlayers: function () {
    player1 = $(game.Players).get(0);
    player2 = $(game.Players).get(1);
    player3 = $(game.Players).get(2);
    player4 = $(game.Players).get(3);

    player1Cards = $(player1.Cards);
    player2Cards = $(player2.Cards);
    player3Cards = $(player3.Cards);
    player4Cards = $(player4.Cards);

    player1Score = player1.Score;
    player2Score = player2.Score;
    player3Score = player3.Score;
    player4Score = player4.Score;
  },

  showPlayerCards: function () {
    $("#player1").empty();
    $(player1Cards).each(function () {
      let color = this.Color;
      let value = this.Value;
      let card = color.charAt(0) + value;
      let li = $("<li class='list-inline-item'></li>");

      if (nextPlayer == p1) {
        li.append("<img src='cards/" + card + ".png'></img>");
      } else {
        if(helleSeite == true){
          li.append("<img src='HanSolo.jpg'></img>");
        }else{
          li.append("<img src='Sturmtruppler.jpg'></img>");
        }
      }

      $("#player1").append(li);
      $("#p1Score").html("Score: " + player1Score);

    });
    $("#player2").empty();
    $(player2Cards).each(function () {
      let color = this.Color;
      let value = this.Value;
      let card = color.charAt(0) + value;
      let li = $("<li class='list-inline-item'></li>");

      if (nextPlayer == p2) {
        li.append("<img src='cards/" + card + ".png'></img>");
      } else {
        if(helleSeite == true){
          li.append("<img src='HanSolo.jpg'></img>");
        }else{
          li.append("<img src='Sturmtruppler.jpg'></img>");
        }
      }
      $("#player2").append(li);
      $("#p2Score").html("Score: " + player2Score);

    });

    $("#player3").empty();
    $(player3Cards).each(function () {
      let color = this.Color;
      let value = this.Value;
      let card = color.charAt(0) + value;
      let li = $("<li class='list-inline-item'></li>");

      if (nextPlayer == p3) {
        li.append("<img src='cards/" + card + ".png'></img>");
      } else {
        if(helleSeite == true){
          li.append("<img src='HanSolo.jpg'></img>");
        }else{
          li.append("<img src='Sturmtruppler.jpg'></img>");
        }
      }
      $("#player3").append(li);
      $("#p3Score").html("Score: " + player3Score);

    });

    $("#player4").empty();
    $(player4Cards).each(function () {
      let color = this.Color;
      let value = this.Value;
      let card = color.charAt(0) + value;
      let li = $("<li class='list-inline-item'></li>");

      if (nextPlayer == p4) {
        li.append("<img src='cards/" + card + ".png'></img>");
      } else {
        if(helleSeite == true){
          li.append("<img src='HanSolo.jpg'></img>");
        }else{
          li.append("<img src='Sturmtruppler.jpg'></img>");
        }
      }
      $("#player4").append(li);
      $("#p4Score").html("Score: " + player4Score);

    });

    INIT.focusOnNextPlayer();

  },

  focusOnNextPlayer: function () {
    console.log("----------------Focus on Next Player-------------")
    $("#topCard").off();
    $("#player1").off();
    $("#player2").off();
    $("#player3").off();
    $("#player4").off();
    if (nextPlayer == p1) {
      if ($("#player1").is(":empty")) {
        $("#p1Score").html("Score: " + "0");
        INIT.endGame();
      }
      $("#player1").on("mouseenter", "li", function (e) {
        $(e.target).addClass("highlight");
      });
      $("#player1").on("mouseleave", "li", function (e) {
        $(e.target).removeClass("highlight");
      });

      $("#player1").one("click", "li", function () {
        let index = $(this).index();
        playedCard = $(player1Cards).get(index);
        $(this).remove();
        INIT.playCard();
      });
      $("#topCard").one("click", "#stack", function () {
        INIT.drawCard();
      });

      $("#player2").off("mouseenter", "li");
      $("#player3").off("mouseenter", "li");
      $("#player4").off("mouseenter", "li");

      $(".name1").addClass("color");
      $(".name2").removeClass("color");
      $(".name3").removeClass("color");
      $(".name4").removeClass("color");
    } else if (nextPlayer == p2) {
      if ($("#player2").is(":empty")) {
        $("#p2Score").html("Score: " + "0");
        INIT.endGame();
      }
      $("#player2").on("mouseenter", "li", function (e) {
        $(e.target).addClass("highlight");
      });
      $("#player2").on("mouseleave", "li", function (e) {
        $(e.target).removeClass("highlight");
      });
      $("#player2").one("click", "li", function () {
        let index = $(this).index();
        playedCard = $(player2Cards).get(index);
        $(this).remove();
        INIT.playCard();

      });
      $("#topCard").one("click", "#stack", function () {
        INIT.drawCard();
      });

      $("#player3").off("mouseenter", "li");
      $("#player4").off("mouseenter", "li");
      $("#player1").off("mouseenter", "li");

      $(".name2").addClass("color");
      $(".name3").removeClass("color");
      $(".name4").removeClass("color");
      $(".name1").removeClass("color");
    } else if (nextPlayer == p3) {
      if ($("#player3").is(":empty")) {
        $("#p3Score").html("Score: " + "0");
        INIT.endGame();
      }
      $("#player3").on("mouseenter", "li", function (e) {
        $(e.target).addClass("highlight");
      });
      $("#player3").on("mouseleave", "li", function (e) {
        $(e.target).removeClass("highlight");
      });
      $("#player3").one("click", "li", function () {
        let index = $(this).index();
        playedCard = $(player3Cards).get(index);
        $(this).remove();
        INIT.playCard();
      });
      $("#topCard").one("click", "#stack", function () {
        INIT.drawCard();
      });

      $("#player4").off("mouseenter", "li");
      $("#player1").off("mouseenter", "li");
      $("#player2").off("mouseenter", "li");

      $(".name3").addClass("color");
      $(".name4").removeClass("color");
      $(".name1").removeClass("color");
      $(".name2").removeClass("color");
    } else if (nextPlayer == p4) {
      if ($("#player4").is(":empty")) {
        $("#p4Score").html("Score: " + "0");
        INIT.endGame();
      }
      $("#player4").on("mouseenter", "li", function (e) {
        $(e.target).addClass("highlight");
      });
      $("#player4").on("mouseleave", "li", function (e) {
        $(e.target).removeClass("highlight");
      });
      $("#player4").one("click", "li", function () {
        let index = $(this).index();
        playedCard = $(player4Cards).get(index);
        $(this).remove();
        INIT.playCard();
      });
      $("#topCard").one("click", "#stack", function () {
        INIT.drawCard();
      });

      $("#player1").off("mouseenter", "li");
      $("#player2").off("mouseenter", "li");
      $("#player3").off("mouseenter", "li");

      $(".name4").addClass("color");
      $(".name1").removeClass("color");
      $(".name2").removeClass("color");
      $(".name3").removeClass("color");
    }
  },

  showTopCard: function () {
    $(".playground .list-inline").fadeIn();
    let color = game.TopCard.Color;
    let value = game.TopCard.Value;
    let card = color.charAt(0) + value;

    let li = $("<li class='list-inline-item'></li>");
    li.append("<img src='cards/" + card + ".png'></img>");
    $("#topCard").append(li);

    let stack = $("<li id='stack' class='list-inline-item'></li>");
    if(helleSeite == true){
      stack.append("<img src='HanSolo.jpg'></img>");
    }else{
      stack.append("<img src='Sturmtruppler.jpg'></img>"); 
    }
    $("#topCard").append(stack);
    INIT.showPlayerCards(1500);

  },

  drawCard: function () {
    console.log("---------------Draw Card----------------")

    $.ajax({
      method: "PUT",
      url: restApi + "/DrawCard/" + gameId,
      contentType: "application/json",
      success: function (data) {
        game = data;
        currentPlayer = data.Player;
        nextPlayer = data.NextPlayer;

        INIT.getPlayer();
      },
      error: function (err) {
        console.log(err);
      }
    });
  },

  getPlayer: function () {
    console.log("---------------Get Player---------------")

    let getP1 = $.ajax({
      method: "GET",
      url: restApi + "/GetCards/" + gameId + "?playerName=" + p1,
      contentType: "application/json",
      error: function (err) {
        console.log(err);
      }
    });

    let getP2 = $.ajax({
      method: "GET",
      url: restApi + "/GetCards/" + gameId + "?playerName=" + p2,
      contentType: "application/json",
      error: function (err) {
        console.log(err);
      }
    });

    let getP3 = $.ajax({
      method: "GET",
      url: restApi + "/GetCards/" + gameId + "?playerName=" + p3,
      contentType: "application/json",
      error: function (err) {
        console.log(err);
      }
    });

    let getP4 = $.ajax({
      method: "GET",
      url: restApi + "/GetCards/" + gameId + "?playerName=" + p4,
      contentType: "application/json",
      error: function (err) {
        console.log(err);
      }
    });

    $.when(getP1, getP2, getP3, getP4).then(function (data1, data2, data3, data4) {

      player1Cards = data1[0].Cards;
      player1Score = data1[0].Score;
      player2Cards = data2[0].Cards;
      player2Score = data2[0].Score;
      player3Cards = data3[0].Cards;
      player3Score = data3[0].Score;
      player4Cards = data4[0].Cards;
      player4Score = data4[0].Score;

      let $slider = document.getElementById('slider');
      if (currentPlayer == p1) {
        if (player1Cards.length == 1) {
          $("#slider").show();
          let isOpen = $slider.classList.contains('slide-in');
          $slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
        }
      }
      if (currentPlayer == p2) {
        if (player2Cards.length == 1) {
          $("#slider").show();
          let isOpen = $slider.classList.contains('slide-in');
          $slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
        }
      }
      if (currentPlayer == p3) {
        if (player3Cards.length == 1) {
          $("#slider").show();
          let isOpen = $slider.classList.contains('slide-in');
          $slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
        }
      }
      if (currentPlayer == p4) {
        if (player4Cards.length == 1) {
          $("#slider").show();
          let isOpen = $slider.classList.contains('slide-in');
          $slider.setAttribute('class', isOpen ? 'slide-out' : 'slide-in');
        }
      }
      INIT.showPlayerCards();
    });

  },

  playCard: function () {

    console.log("------------Play Card--------------")
    currentPlayer = nextPlayer;

    let value = playedCard.Value;
    let color = playedCard.Color;

    if ((value == 13) && (wildColor == null) || (value == 14) && (wildColor == null)) {
      INIT.check();
    } else {
      $.ajax({
        method: "PUT",
        url: restApi + "/PlayCard/" + gameId + "?value=" + value + "&color=" + color + "&wildColor=" + wildColor,
        contentType: "application/json",
        success: function (data) {

          if (data.error == "WrongColor") {
            $(".players #exampleModalCenter").modal();
            INIT.showPlayerCards();
          } else {

            game = data;
            nextPlayer = game.Player;
            if (nextPlayer == p1) {
              player1Cards = game.Cards;
              player1Score = game.Score;
            } else if (nextPlayer == p2) {
              player2Cards = game.Cards;
              player2Score = game.Score;
            } else if (nextPlayer == p3) {
              player3Cards == game.Cards;
              player3Score = game.Score;
            } else if (nextPlayer == p4) {
              player4Cards == game.Cards;
              player4Score = game.Score;
            }
            $(".wildColor").empty();
            $(".wildColor").hide();

            if (wildColor != null) {
              $(".wildColor").show();
              $(".wildColor").append(currentPlayer + " wünscht sich <br>'" + wildColor + "'");
            }
            wildColor = null;
            INIT.getTopCard();
          }
        },
        error: function (err) {
          console.log(err);
        }
      });
    }
  },

  check: function () {
    console.log("-----------------Check Card---------------");
    let check = window.prompt("Geben Sie eine Farbe ein: Red, Blue, Green, Yellow");
    wildColor = check;
    if ((wildColor == "Red") || (wildColor == "Blue") || (wildColor == "Green") || (wildColor == "Yellow")) {
      INIT.playCard();
    } else {
      wildColor = null;
      INIT.check();
    }
  },

  getTopCard: function () {
    console.log("-----------GET TOP CARD-------------");
    $.ajax({
      method: "GET",
      url: restApi + "/TopCard/" + gameId,
      contentType: "application/json",
      success: function (data) {
        topCard = data;

        $("#topCard").empty();
        let color = topCard.Color;
        let value = topCard.Value;
        let card = color.charAt(0) + value;

        let li = $("<li class='list-inline-item'></li>");
        li.append("<img src='cards/" + card + ".png'></img>");
        $("#topCard").append(li);

        let stack = $("<li id='stack' class='list-inline-item'></li>");
        stack.append("<img src='cards/back.png'></img>");
        $("#topCard").append(stack);

        INIT.getPlayer();
      },
      error: function (err) {
        console.log(err);
      }
    });
  },

  endGame: function () {
    $("#startGame").hide();
    $("#endgame").show(2500);
    $(".scores").append("<h1>Danke fürs Spielen!</h1><br>");

    let scores = [];
    scores.push(player1Score, player2Score, player3Score, player4Score);
    scores.sort(function (a, b) {
      return a - b
    });

    $(scores).each(function () {
      if (this == player1Score) {
        $(".scores").append("<h3>" + p1 + ": " + player1Score + "<br>");
      } else if (this == player2Score) {
        $(".scores").append("<h3>" + p2 + ": " + player2Score + "<br>");
      } else if (this == player3Score) {
        $(".scores").append("<h3>" + p3 + ": " + player3Score + "<br>");
      } else if (this == player4Score) {
        $(".scores").append("<h3>" + p4 + ": " + player4Score + "<br>");
      }
    });

    $(".scores").append("<button class='restart2'>Start new Game</button>");
    $(".restart2").click(function () {
      location.reload();
    });
  },

  waehleSeite: function () {
    $("#welcomeBackground").show();
    $("#intro").hide();
    $("#waehleSeite").slideToggle(2500);
    $("#waehleSeite").one("click", "video", function (e) {
      let seite = $(e.currentTarget);
      console.log("Seite: ", seite);
      if (seite.is(dunkleSeite)) {
        console.log("dunkel");
        helleSeite = false;
        dunkleSeite = true;
      } else if (seite.is(helleSeite)) {
        console.log("hell");
        dunkleSeite = false;
        helleSeite = true;
      }
      console.log("HelleSeite: ", helleSeite);
      console.log("DunkleSeite: ", dunkleSeite);

      $("#waehleSeite").fadeOut(2500);
      INIT.onReady();
    })

  },

  start: function () {
    $(".intro").bind("ended", function () {
      INIT.waehleSeite();
    });
  }
}




$(function () {
  INIT.start();
});

$(".restart").click(function () {
  let check = window.prompt("Wenn Sie ein neues Spiel starten möchten tippen Sie 'yes'");
  if (check == "yes") {
    location.reload();
  } else {
    return;
  }
});